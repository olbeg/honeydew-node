var express = require('express');
var cors = require('cors');
var exec = require('exec');
var app = express();
app.use(cors());

app.get('/timestamp', function(req, res) {
    var obj = {};
    obj.date= new Date().toTimeString();

    console.log('params: ' + JSON.stringify(req.params));
    console.log('body: ' + JSON.stringify(req.body));
    console.log('query: ' + JSON.stringify(req.query));

    res.header('Content-type','application/json');
    res.header('Charset','utf8');
    res.send(req.query.callback + '('+ JSON.stringify(obj) + ');');
});

app.post('/honeydew/update', function(req, res) {
    console.log('push delivered');
    exec(['sudo', 'update-azazash.sh'], function(err, out, code) {
        console.log('new revision delivered');
    });
    res.send();
});

app.post('/test', function(req, res) {
    var obj = {};
    obj.id = 1289;
    res.send(JSON.stringify(obj));
});

app.listen(8000);
console.log("Server running at http://127.0.0.1:8000/");
